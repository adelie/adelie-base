0.9.8 (22 Aug 2024)
===================
* Release for beta6

* Change ANSI_COLOR to match our new branding.


0.9.7 (26 Nov 2023)
===================
* Tarball fix for removing .dirkeep files - no change


0.9.6 (22 Oct 2023)
===================
* Release for beta5

* Remove BUG_REPORT_URL, we don't have a usable one for non-reg'd
  contributors right now.  We need to add a page to help for this.


0.9.5 (27 Sep 2020)
===================
* Release for rc2


0.9.4 (26 Jan 2020)
===================
* Release for rc1

* Remove sbin directories from normal user PATH.


0.9.3 (17 Aug 2019)
===================
* Release for beta4

* Do not pretend to be "like" Alpine, we aren't.


0.9.2 (03 May 2019)
===================
* Release for beta3


0.9.2_pre1 (07 Mar 2019)
========================
* Tweak format /etc/issue a bit more.

* Release for experimental beta3 builds.


0.9.1 (16 Dec 2018)
===================
* Release for beta2


0.9.0 (10 Sep 2018)
===================
* Reformat /etc/issue.

* Release for beta1


0.8.1 (01 Aug 2018)
===================
* Create /root directory from Makefile.


0.8.0 (01 Aug 2018)
===================
* Release for beta1 snapshot

* Update /etc/profile to handle shells that don't define $UID


0.7.0 (11 Jun 2018)
===================
* Release for alpha7

* Update MOTD to note Adélie is still in development

* Update codename


0.6.1 (26 Apr 2018)
===================
* Add /etc/inputrc


0.6.0 (26 Apr 2018)
===================
* Release for alpha6


0.5.0 (26 Mar 2018)
===================
* Release for alpha5


0.4.0 (23 Dec 2017)
===================
* Release for alpha4


0.3.4 (12 Dec 2017)
===================
* Add /etc/os-release

* Remove /var/run and replace with symlink to ../run


0.3.3 (28 Jun 2017)
===================
* Remove last vestiges of Portage from /etc/profile


0.3.2 (23 Dec 2016)
===================

* Don't install /dev/{null,tty,zero} static device nodes


0.3.1 (23 Dec 2016)
===================

* Remove /proc/.dirkeep and /sys/.dirkeep files


0.3.0 (23 Dec 2016)
===================

* Add /etc/ld.so.conf to ensure libstdc++ works properly

* Add /etc/lsb-release and /etc/adelie-release files


0.2.2 (11 Dec 2016)
===================

* Change /etc/issue to support agetty(8)


0.2.1 (10 Dec 2016)
===================

* Don't break /etc/mtab symlink during packaging
  (reported by @mc680x0)

* Don't overwrite $UID in /etc/profile


0.2 (24 Oct 2016)
=================

* Initial release.
