===================================================
 Contribution Guide for Adélie Linux Base File Set
===================================================
:Author:
  * **A. Wilcox**, documentation writer
:Copyright:
  © 2016 Adélie Linux.  BSD open source licence.




Introduction
============

This distribution contains the base file set needed for running Adélie Linux.
Its contents are installed to the root file system.


License
```````
The Adélie Linux project typically licenses its original source code under the
permissive, open NCSA license.  However, since the project has found no usable
copies of some files needed for /etc licensed under such a license, it has been
deemed necessary to seek outside projects for these files.

The closest project that met the criteria of freely licensed and usable files
was FreeBSD; therefore, the entire base file set is licensed under the terms of
the 2-clause BSD license.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.




Inclusion in Base File Set
==========================

For a file to be included in the base file set, it **must** meet all of the
following requirements:

#. It must be required by either a standard with which the Adélie Linux
   project is aiming for compliance, or at least 10 packages that are or will
   be shipped in the core repository.

#. It must not be included in any package in the core repository.

#. A file may be included in lieu of requirement #1 if it is required by one
   or more packages included with the ``adelie-base`` metapackage or required
   to boot up all Adélie Linux computers, regardless of init system or CPU
   architecture.





Contributing Changes
====================

This section describes the usual flows of contribution to this repository.


GitLab Pull Requests
````````````````````

#. If you do not already have a GitLab account, you must create one.

#. Create a *fork* of the packages repository.  For more information, consult
   the GitLab online documentation.

#. Clone your forked repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit`` and
   ``git push``.

#. Visit your forked repository in a Web browser.

#. Choose the *Create Pull Request* button.

#. Review your changes to ensure they are correct, and then submit the form.


Mailing List
````````````

#. Clone the packages repository to your computer.

#. Make your changes.

#. Test your changes to ensure they are correct.

#. Add (or remove) changed files using ``git add`` and ``git rm``.

#. Commit your changes to the tree using the command ``git commit``.

#. Use the command ``git format-patch HEAD^`` to create a patch file for your
   commit.

   .. note:: If you have made multiple commits to the tree, you will need to
             add an additional ^ for each commit you have made.  For example,
             if you have made three commits, you will use the command
             ``git format-patch HEAD^^^``.

#. Email the resulting patch to the packagers mailing list.

