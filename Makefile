.POSIX:

all: 

install:
	cp -au tree/* ${DESTDIR}/
	install -d -m700 -o0 -g0 ${DESTDIR}/root

release.tar: tree/*
	tar -cf release.tar --exclude=proc/.dirkeep --exclude=sys/.dirkeep --exclude=release.tar --exclude=.git --format=posix --numeric-owner --owner=0 --group=0 -p --transform="s@^\./@adelie-base-${VERSION}/@" .
