=======================================
 README for Adélie Linux Base File Set
=======================================
:Authors:
 * **A. Wilcox**, maintainer / developer
 * **Elizabeth Myers**, developer
 * **The FreeBSD project and its contributors**, files in /etc
:Status:
 Production
:Copyright:
 © 2016 Adélie Linux.  BSD open source license.



Introduction
============

This distribution contains the base file set needed for running Adélie Linux.
Its contents are installed to the root file system.


License
```````
The Adélie Linux project typically licenses its original source code under the
permissive, open NCSA license.  However, since the project has found no usable
copies of some files needed for /etc licensed under such a license, it has been
deemed necessary to seek outside projects for these files.

The closest project that met the criteria of freely licensed and usable files
was FreeBSD; therefore, the entire base file set is licensed under the terms of
the 3-clause BSD license.


Changes
```````
Any changes to this repository must be reviewed before being pushed to the
master branch.  There are no exceptions to this rule.  For security-sensitive
updates, contact the Security Team at sec-bugs@adelielinux.org.



Background
==========

The ``baselayout`` package from the upstream Gentoo project is deficient in a
number of ways.  It was determined to be fairly inflexible, especially relating
to forcing the installation of a modprobe.d directory with deprecated aliases.
It also installs Gentoo-specific release files (including the
``/etc/gentoo-release`` file and the contents of ``/etc/os-release``), and
makes assumptions about shell environments that may not exist on Adélie.  This
led directly to the creation of this project.



Assumptions and Dependencies
============================

#. It is assumed that the target computer is running a Unix-compatible
   operating environment.

#. It is assumed that there is no other software distribution installed on the
   target computer.  However, an older version of Adélie may be installed.

#. This project must supply an ``/etc/lsb-release`` file compliant with the
   `Linux Standard Base`__, which at the time of this writing is at version
   5.0.
__ http://refspecs.linux-foundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/execenvfhs.html

#. This project must comply with the `Filesystem Hierarchy Standard`__,
   which at the time of this writing is at version 3.0.
__ http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.html




Scope and Limitations
=====================

This section is informational and contains an overall roadmap for the
development of this project.


Initial release
```````````````

Version 1.0 must be fully usable on all Tier I architectures specified for the
Adélie Linux project.  It must render the system operable and compliant with
the Filesystem Hierarchy Standard, version 3.0.

Future versions of 1.x may add support for non-Tier I architectures, if such
support is necessary in this project.  It is not anticipated to include any
architecture- or machine-specific objects in this project.


Secondary release
`````````````````

If user demand is present for a port of the Adélie system to a different kernel
such as FreeBSD is present, changes to this project to ensure compatibility
with such a system may begin during the 2.x version cycle.


Subsequent releases
```````````````````

Newer versions of the LSB or FHS standards may require tweaks or changes to
this project.  They will be reviewed and the changes required will be
determined if and when such standard updates are published.


Limitations and exclusions
``````````````````````````

The following portions of the FHS and LSB are purposefully ignored by this
project, for the reason specified.

Executables in ``/bin``
:::::::::::::::::::::::
The executables specified in FHS §3.4.2 and §3.4.3 are excluded from this
project as it is assumed these executables are installed by other packages
included in the base software distribution.

The single executable specified in FHS §6.1.2 is specified in a way that makes
it ambiguous if it is always required or only required on computers containing
a serial port.  Either way, this executable is provided by a separate package
from a separate project and is also excluded from this project.

Files in ``/etc``
:::::::::::::::::
Some files specified as optional in FHS §3.7.3 are not included in this project
as they are better handled by their respective packages, including:

* ``X11`` - handled by the ``xorg-server`` package.

* ``csh.login`` - handled by the ``tcsh`` package.

* ``exports`` - handled by the ``nfs-utils`` package.

* ``fstab`` and ``resolv.conf`` - handled by Project Horizon.

* ``ftpusers`` - handled by FTP daemon packages.

* ``gateways`` - handled by the ``netkit-routed`` package.

* ``gettydefs`` - handled by the ``mgetty`` or ``mingetty`` package.

* ``group`` and ``passwd`` - handled by the ``shadow`` package.

* ``host.conf`` - this file is glibc-specific.

* ``hosts.allow`` and ``hosts.deny`` - handled by the ``tcp-wrappers`` package.

* ``hosts.equiv`` - handled by the ``netkit-rsh`` package.

* ``hosts.lpd`` and ``printcap`` - handled by printer packages.

* ``inetd.conf`` - handled by inetd packages.

* ``inittab`` - handled by the ``sysvinit`` package.

* ``mtools.conf`` - handled by the ``mtools`` package.

* ``rpc`` - handled by the ``librpc`` package.

* ``shells`` - automatically generated by ``apk`` on shell installation.

* ``syslog.conf`` - handled by syslog packages that require it.

``/etc/cron.*`` tree
::::::::::::::::::::
This set of directories as specified in LSB-Core §18.2 is not included in this
project, as any package that satisfies the ``cron`` virtual shall create it.

``/etc/init.d`` directory
:::::::::::::::::::::::::
This directory specifed in LSB-Core §18.2 is created by the ``openrc`` package
and is not included in this project.

.. warning::
   Do we know what s6 does?

The ``/etc/mtab`` file
::::::::::::::::::::::
The ``/etc/mtab`` file is provided as a symlink to /proc/self/mounts only.
Support for the ``/etc/mtab`` file being a regular file is not provided.
This appears to be in line with FHS §3.7.3.

Files in ``/lib``
:::::::::::::::::
While FHS §3.9.2 is ambiguous on whether ``/lib/libc.so.*`` is required when
``/lib/ld*`` is present or not, this project *does* supply a symlink called
``/lib/libc.so.1``.

It is assumed that the symlink ``/lib/cpp`` as specified in FHS §3.9.2 is
provided by ``gcc``.

The directory ``/lib/modules`` specified by FHS §3.9.3 is provided by the
``easy-kernel-modules`` package on systems where it is installed.

Executables in ``/sbin``
::::::::::::::::::::::::
The executables specified in FHS §3.16.2 and §3.16.3 are excluded from this
project as it is assumed these executables are installed by other packages
included in the base software distribution.

